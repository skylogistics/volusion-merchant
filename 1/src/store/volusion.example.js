import SkyboxSDK from '@skyboxcheckout/merchant-sdk';
const __cnStore = require('../../config/store.json');
const api = require('../xhr');
const mivaStyles = require('../../assets/css/volusion.css');

window.Sdk = new SkyboxSDK({
    IDSTORE: __cnStore.IDSTORE,
    MERCHANT: __cnStore.MERCHANT,
    MERCHANTCODE: __cnStore.MERCHANTCODE,
    STORE_URL: __cnStore.STORE_URL,
    SUCCESSFUL_PAGE: __cnStore.SUCCESSFUL_PAGE,
    CHECKOUT_PAGE: __cnStore.CHECKOUT_PAGE,
    CHECKOUT_BUTTON_CLASS: __cnStore.CHECKOUT_BUTTON_CLASS
});

(function () {
    Sdk.Common().initChangeCountry();    
})();
